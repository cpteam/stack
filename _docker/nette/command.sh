#!/usr/bin/env bash

writableDir() {
    if [ -e $1 ]
    then
        echo "Temp already exists\n"
    else
        mkdir $1 -m 775
    fi

    chmod 775 -R $1
    chown dockeruser:dockeruser -R $1

    cd $1 && umask 002
}

ROOT=/var/docker

DIRECTORY_TEMP=$ROOT/temp
DIRECTORY_WWW_TEMP=$ROOT/www/assets
DIRECTORY_LOG=$ROOT/log

ADMINER_PATH=$ROOT/adminer.php
ADMINER_WWW_PATH=$ROOT/www/adminer.php

writableDir $DIRECTORY_WWW_TEMP
writableDir $DIRECTORY_LOG

writableDir $DIRECTORY_TEMP
writableDir $DIRECTORY_TEMP/cache

cd $ROOT && composer install --no-interaction

rm -rf $DIRECTORY_TEMP/cache/*

if [ $DEBUG_MODE = "1" ]
then
	if [ -e $ADMINER_WWW_PATH ]; then
		echo "Adminer already exists"
	else
		ln -s $ADMINER_PATH $ADMINER_WWW_PATH
	fi
else
	if [ -e $ADMINER_WWW_PATH ]; then
		rm $ADMINER_WWW_PATH
	fi
fi

if [ $DEBUG_MODE = "1" ]; then
    echo "Debug Mode: ENABLED"

    {
    	echo "<?php declare(strict_types = 1);"
    	echo "return true;"
    } > $ROOT/src/debugMode.php
else
    echo "Debug Mode: DISABLED"

       {
    	echo "<?php declare(strict_types = 1);"
    	echo "return false;"
    } > $ROOT/src/debugMode.php
fi


php www/index.php migr:cont

php-fpm


