#!/usr/bin/env bash

cd www/assets && umask 002 && cd ../../

npm install && npm cache clean

npm link webpack

npm run build
npm run watch
