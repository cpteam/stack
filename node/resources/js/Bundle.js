import libToastr from 'toastr';

//const toast =

export default {

	toast: (msg, type = 'info', pernament = false) => {


		let el = document.createElement('div');
		el.className = 'toast ' + type + (pernament ? ' pernament' : '');


		let text = document.createElement('div');
		text.className = 'text';
		text.textContent = msg;

		el.appendChild(text);

		if (pernament === true) {
			let x = document.createElement('div');

			x.className = 'times';
			x.textContent = '×';

			x.addEventListener('click', () => {
				el.remove();
			});

			el.appendChild(x);
		} else {
			let timeout;

			const pushTimeout = () => {
				return setTimeout(() => {
					el.remove();
				}, 6000);
			};

			timeout = pushTimeout();

			el.addEventListener('mouseover', () => {
				console.log('aloha');

				clearTimeout(timeout);
			});

			el.addEventListener('mouseout', () => {
				console.log('aloha out');

				timeout = pushTimeout();
			});
		}

		document.getElementById('bundle-toast-content')
			.appendChild(el);

		return false;
	},
};
