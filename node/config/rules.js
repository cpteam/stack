
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports.lessLoader = {
	test: /\.(less)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader', 'less-loader'],
	}),
};

module.exports.sassLoader = {
	test: /\.(scss|sass)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'resolve-url-loader', 'postcss-loader', 'sass-loader'],
	}),
};

module.exports.cssLoader = {
	test: /\.(css)$/,
	use: ExtractTextPlugin.extract({
		fallback: 'style-loader',
		use: ['css-loader', 'postcss-loader'],
	}),
};

module.exports.fontLoader = {
	test: /\.(eot|svg|ttf|woff|woff2)$/,
	loader: 'file-loader?name=[hash:5].[ext]',
};

module.exports.jqueryLoader = {
	test: require.resolve('jquery'),
	use: [
		{
			loader: 'expose-loader',
			options: 'jQuery',
		},
		{
			loader: 'expose-loader',
			options: '$',
		},
	],
};

module.exports.jsxLoader = {
	test: /\.(js|jsx)$/,
	exclude: /node_modules/,
	use: [
		{
			loader: 'babel-loader',
		},
	],
};

module.exports.imageLoader = {
	test: /\.(jpg|png)$/,
	use: [
		{
			loader: 'url-loader',
			options: {
				limit: 2500,
			},
		},
	],
};

