const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const {
	fontLoader,
	imageLoader,
	jsxLoader,
	lessLoader,
	sassLoader,
	cssLoader,
	jqueryLoader,
} = require('./rules');

module.exports = {
	entry: {
		bundle: __dirname + '/../resources/index.js',
	},
	output: {
		path: path.resolve(__dirname, './../www'),
		filename: '[name].js',
		chunkFilename: '[name].chunk.js',
	},
	module: {
		rules: [
			fontLoader,
			imageLoader,
			jsxLoader,
			lessLoader,
			sassLoader,
			cssLoader,
			jqueryLoader,
			{
				test: require.resolve('toastr'),
				use: [
					{
						loader: 'expose-loader',
						options: 'toastr',
					},
				],
			},
			{
				test: require.resolve('nette-forms'),
				use: [
					{
						loader: 'expose-loader',
						options: 'Nette',
					},
				],
			},
			{
				test: require.resolve('react'),
				use: [
					{
						loader: 'expose-loader',
						options: 'React',
					},
				],
			},
			{
				test: require.resolve('react-dom'),
				use: [
					{
						loader: 'expose-loader',
						options: 'ReactDOM',
					},
				],
			},
		],
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			children: true,
			minChunks: 2,
			async: true,
		}),
		new webpack.optimize.OccurrenceOrderPlugin(true),
		new webpack.LoaderOptionsPlugin({
			//minimize: true,
			minimize: false,
			debug: true,
		}),

		// Extract the CSS into a separate file
		new ExtractTextPlugin('styles.css'),

		//new webpack.optimize.UglifyJsPlugin({
		//    compressor: {
		//        //warnings: false,
		//        warnings: true,
		//    },
		//}),
	],
};
