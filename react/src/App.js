import React, {Component} from 'react';


import styles from './scss/styles.scss';

import Dialog from './components/Dialog';
import Step from './components/Step';
import Button from './components/Button';

import promoLogo from './../images/animefest/af_2017_white.png';

import FileInput from './components/FileInput';

import Header from './components/Header';

import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

import Cosplan from './components/Cosplan';

import Section from './components/Section';



const ENTER_KEY = 'Enter';

const dayName = [
	'Pátek',
	'Sobota',
	'Neděle',
];


const cropSettings = {
	width: 882,
	aspect: 882 / 1332,
	height: 1332,
};

//const backgrounds = {
//	1: require('./../images/layout/1.png'),
//	2: require('./../images/layout/2.png'),
//	3: require('./../images/layout/3.png'),
//	4: require('./../images/layout/4.png'),
//	6: require('./../images/layout/6.png'),
//	7: require('./../images/layout/7.png'),
//};



//const font = require('./../images/fontNickname/font.ttf');

export default class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			showBasic: false,

			stepsError: {
				1: false,
				2: false,
				3: false,
				4: false,
				5: false,
				6: false,
				7: false,
				8: false,
				9: false,
			},

			promoName: '',
			promoDays: {
				0: {id: 0, bit: 1, cls: 'canvas-0', dayName: 'Pá', value: false, name: '', photo: null, canvas: null, ctx: null},
				1: {id: 1, bit: 2, cls: 'canvas-1', dayName: 'So', value: false, name: '', photo: null, canvas: null, ctx: null},
				2: {id: 2, bit: 4, cls: 'canvas-2', dayName: 'Ne', value: false, name: '', photo: null, canvas: null, ctx: null},
			},

			promoPermalink: '',

			currentStep: 1,

			callback: false
		};

		this.callback = false;

		this.canvas = [null, null, null];
		this.ctx = [null, null, null];

		this.characterName = ['', '', ''];

		this.data = {
			0: null,
			1: null,
			2: null,
		};

	}

	nextStep() {
		const {currentStep, promoDays} = this.state;

		let nextStep = currentStep + 1;

		//
		//if (nextStep === 3) {
		//	const bit =
		//		(this.state.promoDays[0].value === true ? this.state.promoDays[0].bit : 0) +
		//		(this.state.promoDays[1].value === true ? this.state.promoDays[1].bit : 0) +
		//		(this.state.promoDays[2].value === true ? this.state.promoDays[2].bit : 0);
		//
		//
		//	let preloadImage = new Image();
		//	preloadImage.src = backgrounds[bit];
		//}


		// Skip Friday if is disabled
		if (nextStep === 3 && promoDays[0].value === false) {
			nextStep += 2;
		}

		// Skip Saturday if is disabled
		if (nextStep === 5 && promoDays[1].value === false) {
			nextStep += 2;
		}

		// Skip Sunday if is disabled
		if (nextStep === 7 && promoDays[2].value === false) {
			nextStep += 2;
		}


		if (currentStep === 3) {
			this.setState({
				promoDays: {
					...this.state.promoDays,
					[0]: {
						...this.state.promoDays[0],
						photo: this.canvas[0].toDataURL(),
					},
				},
			});
		}

		if (currentStep === 5) {
			this.setState({
				promoDays: {
					...this.state.promoDays,
					[1]: {
						...this.state.promoDays[1],
						photo: this.canvas[1].toDataURL(),
					},
				},
			});
		}

		if (currentStep === 7) {
			this.setState({
				promoDays: {
					...this.state.promoDays,
					[2]: {
						...this.state.promoDays[2],
						photo: this.canvas[2].toDataURL(),
					},
				},
			});
		}


		this.setState({currentStep: nextStep});

		const method = () => {
			const cnvs0 = document.getElementById('canvas');
		const ctx0 = cnvs0.getContext('2d');

		// hold top-right hand corner when rotating
		ctx0.translate(cnvs0.width - 1, 0);

		// // rotate 270 degrees
		ctx0.rotate(3 * Math.PI / 2);

		ctx0.textAlign = 'right';
		ctx0.fillStyle = '#FFF';

		const name1 = this.state.promoDays[0].name.toUpperCase();
		const name2 = this.state.promoDays[1].name.toUpperCase();
		const name3 = this.state.promoDays[2].name.toUpperCase();

		const bit =
			(this.state.promoDays[0].value === true ? this.state.promoDays[0].bit : 0) +
			(this.state.promoDays[1].value === true ? this.state.promoDays[1].bit : 0) +
			(this.state.promoDays[2].value === true ? this.state.promoDays[2].bit : 0);


		//console.warn('bit', bit);

		let map = [null, null, null];

		if (bit === 1) {
			map = [null, name1, null];
		}

		if (bit === 2) {
			map = [null, name2, null];
		}
		if (bit === 4) {
			map = [null, name3, null];
		}

		if (bit === 3) {
			map = [name1, name2, null];
		}

		if (bit === 6) {
			map = [null, name2, name3];
		}
		if (bit === 7) {
			map = [name1, name2, name3];
		}

		const size = (l) => {
			if (l >= 26) return 16;
			if (l >= 24) return 18;
			if (l >= 22) return 20;
			if (l >= 20) return 22;
			if (l >= 18) return 24;
			if (l >= 16) return 26;
			if (l >= 14) return 28;
			if (l >= 12) return 30;
			if (l >= 10) return 33;
			return 36;
		};

		const sizeMid = (l) => {
			if (l >= 26) return 20;
			if (l >= 25) return 21;
			if (l >= 24) return 22;
			if (l >= 23) return 23;
			if (l >= 22) return 24;
			if (l >= 21) return 25;
			if (l >= 20) return 26;
			if (l >= 19) return 27;
			if (l >= 18) return 28;
			if (l >= 17) return 30;
			if (l >= 16) return 32;
			if (l >= 14) return 34;
			if (l >= 12) return 36;
			if (l >= 10) return 39;
			return 42;
		};

		if (map[0]) {
			ctx0.font = size(map[0].length) + 'px "Raleway"';
			ctx0.fillText(map[0], -175, -830);
		}

		if (map[1]) {

			ctx0.font = sizeMid(map[1].length) + 'px "Raleway"';
			ctx0.fillText(map[1], -120, -440);
		}

		if (map[2]) {
			//console.log(map[2].length);

			ctx0.font = size(map[2].length) + 'px "Raleway"';
			ctx0.fillText(map[2], -175, -90);
		}
		// ctx.save();
		ctx0.restore();

		let permalink = '';

		let data = new FormData();
		data.append('image', cnvs0.toDataURL());
		data.append('name', this.state.promoName);

		data.append('chars', JSON.stringify({
			0: {
				value: this.state.promoDays[0].value,
				name: this.state.promoDays[0].name,
				//photo: this.state.promoDays[0].photo,
			}, 1: {
				value: this.state.promoDays[1].value,
				name: this.state.promoDays[1].name,
				//photo: this.state.promoDays[1].photo,
			}, 2: {
				value: this.state.promoDays[2].value,
				name: this.state.promoDays[2].name,
				//photo: this.state.promoDays[2].photo,
			},
		}));

		fetch('https://www.cosplan.cz/api?do=promoAnimefest', {
			method: 'POST',
			body: data,
		}).then(function (response) {
			return response.json();
		}).then((body) => {
			//console.log(body);

			this.setState({
				currentStep: 10,
				cosplanImage: cnvs0.toDataURL(),
				promoPermalink: body.permalink,
			});
		}).catch(() => {
			this.setState({
				currentStep: 10,
				cosplanImage: cnvs0.toDataURL(),
			});
		});
	}

		if (nextStep === 9) {
			setTimeout(() => {
				method()
			}, 5000);
		}
	}

	backStep() {
		const {currentStep, promoDays} = this.state;

		let backStep = currentStep - 1;

		// Skip Friday if is disabled
		if (backStep === 4 && promoDays[0].value === false) {
			backStep -= 2;
		}

		// Skip Saturday if is disabled
		if (backStep === 6 && promoDays[1].value === false) {
			backStep -= 2;
		}

		// Skip Sunday if is disabled
		if (backStep === 8 && promoDays[2].value === false) {
			backStep -= 2;
		}

		this.setState({currentStep: backStep});
	}

	componentDidUpdate() {
	}

	componentDidMount() {
		window.addEventListener('load', (e) => {
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments);
					}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m);
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-77331866-5', 'auto');
			ga('send', 'pageview');


			const delay = 500;

			setTimeout(() => {
				this.setState({showBasic: true});
			}, delay);


			this.canvas[0] = document.getElementById('canvas-0');
			this.canvas[1] = document.getElementById('canvas-1');
			this.canvas[2] = document.getElementById('canvas-2');

			this.ctx[0] = this.canvas[0].getContext('2d');
			this.ctx[1] = this.canvas[1].getContext('2d');
			this.ctx[2] = this.canvas[2].getContext('2d');

		});
	}

	componentWillUnmount() {
	}

	onInputNameEnter() {
		const {promoName, stepsError} = this.state;

		if (promoName.length) {
			this.setState({
				stepsError: {
					...stepsError,
					[1]: false,
				},
			});

			this.nextStep();
		} else {
			this.setState({
				stepsError: {
					...stepsError,
					[1]: true,
				},
			});
		}
	};


	fileInputOnChange(index, e) {
		let {promoDays} = this.state;

		const {currentStep, stepsError} = this.state;

		const fileList = e.target.files;
		if (fileList.length) {
			const reader = new FileReader();
			reader.onloadend = () => {
				promoDays[index].photo = reader.result;

				//console.log(this.state);
				this.setState({
					promoDays: promoDays,
					stepsError: {
						...stepsError,
						[currentStep]: false,
					},
				});
			};
			reader.onerror = () => {
				console.error('File upload error');
			};
			reader.readAsDataURL(fileList[0]);
		}
	}

	writeToCanvas(index, image, cropX, cropY, cropWidth, cropHeight, cWidth, cHeight) {
		const day = this.state.promoDays[index];

		this.canvas[index].width = cWidth;
		this.canvas[index].height = cHeight;

		this.ctx[index].clearRect(0, 0, cWidth, cHeight);
		this.ctx[index].drawImage(image, cropX, cropY, cropWidth, cropHeight, 0, 0, cWidth, cHeight);

	}

	render() {
		const fixedBarHeight = '100px';

		const visibility = {
			visibility: 'hidden',
			display: 'none',
		};

		const {
			showBasic,
			promoName,
			currentStep,
			promoDays,
			minHeight,
		} = this.state;

		const inputStyle = {
			border: '1px solid grey',
			padding: '10px',
			width: '100%',
			minHeight,
		};

		const crop = (index) => {

			const day = this.state.promoDays[index];

			const {stepsError, currentStep} = this.state;

			const method = (percentCrop, crop) => {
				const img = document.getElementsByClassName('ReactCrop__image')[0];

				const cWidth = cropSettings.width;
				const cHeight = cropSettings.height;

				this.writeToCanvas(index, img, crop.x, crop.y, crop.width, crop.height, cWidth, cHeight);
			};

			return (
				<div className={styles.cropper}>
					<Section center height={140}>
						{this.state.stepsError[currentStep] && <div className={styles.msg + ' ' + styles.error}>
							Nejprve vyber obrazek
						</div>}

						<Header size={16}>Obrázek postavy</Header>
						{
							day.photo === null && <FileInput onChange={(e) => this.fileInputOnChange(index, e)}/>
							||
							day.photo && <ReactCrop
								src={day.photo}
								crop={cropSettings}
								onImageLoaded={(percentCrop, image, crop) => method(percentCrop, crop)}
								onComplete={(percentCrop, crop) => method(percentCrop, crop)}
							/>
						}
					</Section>
					<Section center>
						<Button red onClick={(e) => this.backStep()}>Zpět</Button>
						<Button green onClick={(e) => {
							if (day.photo === null) {
								this.setState({
									stepsError: {
										...stepsError,
										[currentStep]: true,
									},
								});
							} else {
								this.setState({
									stepsError: {
										...stepsError,
										[currentStep]: false,
									},
								});
								this.nextStep();
							}
						}
						}>
							Další
						</Button>
					</Section>
				</div>
			);
		};

		const characterName = (index) => {
			const day = this.state.promoDays[index];

			return (
				<div className={styles.cropper}>
					<Section height={140}>
						{this.state.stepsError[currentStep] && <div className={styles.msg + ' ' + styles.error}>
							Nejprve napiš jméno postavy (1 až 26 znaků)
						</div>}

						<Header size={20}>Jméno postavy</Header>
						<input type='text'
							   placeholder="Jméno postavy"
							   style={inputStyle} value={day.name} onInput={(e) => {
							this.setState({
								promoDays: {
									...this.state.promoDays,
									[index]: {
										...day,
										name: e.target.value,
									},
								},
							});
						}}/>
					</Section>
					<Section center>
						<Button red onClick={(e) => this.backStep()}>Zpět</Button>
						<Button green onClick={(e) => {
							const {currentStep, stepsError} = this.state;

							if (day.name.length && day.name.length <= 26) {
								this.setState({
									stepsError: {
										...stepsError,
										[currentStep]: false,
									},
								});

								this.nextStep();
							} else {
								this.setState({
									stepsError: {
										...stepsError,
										[currentStep]: true,
									},
								});
							}
						}}
						>
							Další
						</Button>
					</Section>
				</div>
			);
		};


		const {stepsError} = this.state.stepsError;

		return (
			<div className={styles.wrap} style={{
				backgroundImage: 'url(' + promoLogo + ')',
				fontFamily: 'Panton',
			}}>

				<Dialog
					show={showBasic}
					padding={25}
					step={currentStep}

					minHeight={200}
				>

					<Step step={1}>
						<h2>Napiš svojí přezdívku</h2>
						<Section height={140}>
							{this.state.stepsError[1] && <div className={styles.msg + ' ' + styles.error}>
								Nejprve napiš svojí přezdívku.
							</div>}
							<input
								style={inputStyle}
								ref={(e) => this.inputName = e}
								onInput={(e) => this.setState({promoName: e.target.value})}
								type="text"
								placeholder="Přezdívka"
								value={promoName}
								onKeyPress={(e) => {
									if (e.key === ENTER_KEY) {
										() => this.onInputNameEnter();
									}
								}}
								className={styles.marginTop30}
							/>
						</Section>
						<div className={styles.flexWrap + ' ' + styles.marginTop40}>
							<Button green onClick={() => this.onInputNameEnter()}>Další</Button>
						</div>
					</Step>

					<Step step={2}>
						<h2>Vyber dny</h2>
						<Section height={140}>
							{this.state.stepsError[2] && <div className={styles.msg + ' ' + styles.error}>
								Nejprve vyber dny.
							</div>}
							<div className={styles.flexWrap + ' ' + styles.spaceAround + ' ' + styles.marginTop30}>
								{Object.keys(promoDays).map((key, index) => {
									const day = this.state.promoDays[key];
									return (
										<div key={index} ref={day}
											 className={styles.day + ' ' + (day.value === true && styles.selected)}
											 onClick={(e) => {
												 this.setState({
													 promoDays: {
														 ...promoDays,
														 [day.id]: {
															 ...day,
															 value: !day.value,
														 },
													 },
												 });
											 }}
										>
											{day.dayName}
										</div>
									);
								})}
							</div>
						</Section>
						<div className={styles.flexWrap + ' ' + styles.marginTop40}>
							<Button red onClick={(e) => this.backStep()}>Zpět</Button>
							<Button green onClick={(e) => {
								let any = false;
								{
									Object.keys(promoDays).map((key, index) => {
										const day = promoDays[key];

										if (day.value === true) {
											//console.warn('next');
											any = true;

											this.setState({
												stepsError: {
													...stepsError,
													[this.state.currentStep]: false,
												},
											});

											this.nextStep();

											return;
										}
									});
								}
								if (any === false) {

									this.setState({
										stepsError: {
											...stepsError,
											[this.state.currentStep]: true,
										},
									});

									//console.log(this.state);
								}
							}}>
								Další
							</Button>
						</div>
					</Step>
					<Step step={3}>
						<h2>{dayName[0]}</h2>
						{crop(0)}
					</Step>
					<Step step={4}>
						<h2>{dayName[0]}</h2>
						{characterName(0)}
					</Step>

					<Step step={5}>
						<h2>{dayName[1]}</h2>
						{crop(1)}
					</Step>
					<Step step={6}>
						<h2>{dayName[1]}</h2>
						{characterName(1)}
					</Step>

					<Step step={7}>
						<h2>{dayName[2]}</h2>
						{crop(2)}
					</Step>
					<Step step={8}>
						<h2>{dayName[2]}</h2>
						{characterName(2)}
					</Step>

					<Step step={9}>
						<div style={{
							display: 'flex',
							alignItems: 'center',
							justifyContent: 'center',
						}}>
							<Header size={32} center marginTop={50}>

								<img src={require('./../images/ripple.svg')}/>
								<div>Čekejte prosím</div>
							</Header>
						</div>

						<Cosplan
							style={visibility}
							name={this.state.promoName}
							width={1200}
							height={630}
							ratio={1 / 3}
							days={promoDays}
							id="canvas"
						/>

					</Step>
					<Step step={10}>
						{/*<ImageBox src={this.state.cosplanImage}/>*/}
						<div className={styles.result}>
							<img src={this.state.cosplanImage}/>
						</div>

						<Section flex spaceAround>

							<a href={this.state.cosplanImage} download="cosplan-animefest2017">
								Stáhnout
							</a>
							{this.state.promoPermalink.length > 0 &&
								<a href={this.state.promoPermalink} target="_blank">Trvalý odkaz</a>
							}

							{this.state.promoPermalink.length > 0 &&
								<a href={this.state.promoPermalink} target="_blank">Otevřít na Cosplánu</a>
							}

						</Section>
					</Step>

				</Dialog>

				<canvas id='canvas-0' width={cropSettings.width} height={cropSettings.height} style={visibility}/>
				<canvas id='canvas-1' width={cropSettings.width} height={cropSettings.height} style={visibility}/>
				<canvas id='canvas-2' width={cropSettings.width} height={cropSettings.height} style={visibility}/>

			</div>
		);
	}
}
