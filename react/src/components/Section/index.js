import React, {Component} from 'react';
import styles from './section.scss';


export default class Section extends Component {


	render() {
		const {
			src,
			height,
			width,
			center,
			right,
			spaceAround,
			spaceBetween,
			flex,
		} = this.props;


		const classes =
			(flex && styles.flex || styles.noFlex)
			+ ' ' + (center && styles.center)
			+ ' ' + (right && styles.right)
			+ ' ' + (spaceBetween && styles.spaceBetween)
			+ ' ' + (spaceAround && styles.spaceAround)
		;

		const style = {
			minHeight: height + 'px',
		};

		return (
			< div;
		className = {
			styles.section;
	}>
	<
		div;
		className = {classes};
		style = {style} >
			{this.props.children;
	}
	</
		div >
		< / div >
	)
	}
}
