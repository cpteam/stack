import React, {Component} from 'react';
import {connect} from 'react-redux';
import styles from './fileInput.sass';

class FileInput extends Component {

	render() {

		const {
			theme,
			onChange,
		} = this.props;

		return (
			< div;
		className = {styles.wrap + ' ' + theme;
	}>
	<
		input;
		type = 'file';
		name = 'photo';
		onChange = {onChange} / >
			< div;
		className = {styles.text;
	}>
		Přetáhni;
		sem;
		obrázek
		< div;
		className = {styles.or;
	}>
		nebo < / div >
		< div;
		className = {styles.btn;
	}>
		Vyber;
		soubor < / div >
		< / div >
		< / div >
	)
	}
}


function mapStateToProps(state) {
	return {};
}

function mapDispatchToProps(dispatch) {
	return {};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(FileInput);
