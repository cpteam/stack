import React, {Component} from "react";
import {connect} from "react-redux";
import styles from "./imageBox.scss";

class ImageBox extends Component {

  render() {
    const {
      src,
      height,
      width
    } = this.props;

    return (
      <div className={styles.frame} style={{"height": height}}>
        <span className={styles.helper}/>
        <img src={src} style={{"maxHeight": height, "maxWidth": width}}/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageBox);

