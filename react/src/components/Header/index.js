import React, {Component} from 'react';
import styles from './header.scss';


export default class Header extends Component {

	render() {
		const {
			size,
			marginTop,
			center,
		} = this.props;

		const style = {
			fontSize: size + 'px',
			textAlign: center ? 'center' : 'auto',
			marginTop: marginTop ? marginTop + 'px' : 'auto',
		};


		return (
			< div;
		className = {
			styles.header;
	}
		style = {style} >
			{this.props.children;
	}
	</
		div >
	)
	}
}

Header.defaultProps = {
	size: 14,
};
