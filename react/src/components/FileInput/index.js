import React, {Component} from 'react';
import styles from './fileInput.sass';

import Button from "./../Button";

export default class FileInput extends Component {

  render() {

    const {
      name,
      theme,
      onChange
    } = this.props;

    return (
      <div className={styles.wrap + ' ' + theme}>
        <input type="file" name={name} onChange={onChange}/>
        <div className={styles.text}>Přetáhni sem obrázek
          <div className={styles.or}>nebo</div>
          <Button red margin={0} size={10}>Vyber soubor</Button>
        </div>
      </div>
    )
  }
}

