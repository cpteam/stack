import React, {Component} from 'react';
import {connect} from 'react-redux';
import styles from './colorPicker.sass';

class ColorPicker extends Component {

	render() {
		const {
			options,
			onChange,
		} = this.props;

		const colors = [
			{name: 'RED', color: '#d32f2f'},
			{name: 'PURPLE', color: '#7B1FA2'},
			{name: 'INDIGO', color: '#303F9F'},
			{name: 'BLUE', color: '#1976D2'},
			{name: 'LIGHT-BLUE', color: '#0288D1'},
			{name: 'CYAN', color: '#0097A7'},
			{name: 'TEAL', color: '#00796B'},
			{name: 'GREEN', color: '#388E3C'},
			{name: 'LIGHT-GREEN', color: '#689f38'},
			{name: 'LIME', color: '#AFB42B'},
			{name: 'YELLOW', color: '#fbc02d'},
			{name: 'AMBER', color: '#FFA000'},
			{name: 'ORANGE', color: '#F57C00'},
			{name: 'DEEP-ORANGE', color: '#E64A19'},
			{name: 'BROWN', color: '#5D4037'},
			{name: 'GREY', color: '#616161'},
			{name: 'BLUE-GREY', color: '#455A64'},
		];

		return (
			< div >
			< select;
		onChange = {(event) =
	>
		{
			if (event.target.value) {
				event.target.style.background = event.target.value;
				event.target.style.color = '#FFF';
				event.target.style.fontWeight = 'bold';
			} else {
				event.target.style.background = '';
				event.target.style.color = '';
				event.target.style.fontWeight = '';
			}
		}
	}
		name = 'color'
			>
			< option;
		value = '' > Automaticky < / option >
			{colors.map(item = >
			< option;
		key = {item.name;
	}
		style = {
		{
			background: item.color;
		}
	}
		className = {styles.colorOpt;
	}
		value = {item.color;
	}
	>
		{
			item.name;
		}
	</
		option >
	)
	}
	</
		select >
		< / div >
	)
	}
}


function mapStateToProps(state) {
	return {};
}

function mapDispatchToProps(dispatch) {
	return {};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ColorPicker);


// <option style={{"background": "#d32f2f"}} className={styles.colorOpt}
//         value="#d32f2f">RED
// </option>
// <option style={{"background": "#7B1FA2"}} className={styles.colorOpt}
//         value="#7B1FA2">PURPLE
// </option>
// <option style={{"background": "#303F9F"}} className={styles.colorOpt}
//         value="#303F9F">INDIGO
// </option>
// <option style={{"background": "#1976D2"}} className={styles.colorOpt}
//         value="#1976D2">BLUE
// </option>
// <option style={{"background": "#0288D1"}} className={styles.colorOpt}
//         value="#0288D1">LIGHT-BLUE
// </option>
// <option style={{"background": "#0097A7"}} className={styles.colorOpt}
//         value="#0097A7">CYAN
// </option>
// <option style={{"background": "#00796B"}} className={styles.colorOpt}
//         value="#00796B">TEAL
// </option>
// <option style={{"background": "#388E3C"}} className={styles.colorOpt}
//         value="#388E3C">GREEN
// </option>
// <option style={{"background": "#689f38"}} className={styles.colorOpt}
//         value="#689f38">LIGHT-GREEN
// </option>
// <option style={{"background": "#AFB42B"}} className={styles.colorOpt}
//         value="#AFB42B">LIME
// </option>
// <option style={{"background": "#fbc02d"}} className={styles.colorOpt}
//         value="#fbc02d">YELLOW
// </option>
// <option style={{"background": "#FFA000"}} className={styles.colorOpt}
//         value="#FFA000">AMBER
// </option>
// <option style={{"background": "#F57C00"}} className={styles.colorOpt}
//         value="#F57C00">ORANGE
// </option>
// <option style={{"background": "#E64A19"}} className={styles.colorOpt}
//         value="#E64A19">DEEP-ORANGE
// </option>
// <option style={{"background": "#5D4037"}} className={styles.colorOpt}
//         value="#5D4037">BROWN
// </option>
// <option style={{"background": "#616161"}} className={styles.colorOpt}
//         value="#616161">GREY
// </option>
// <option style={{"background": "#455A64"}} className={styles.colorOpt}
//         value="#455A64">BLUE-GREY
