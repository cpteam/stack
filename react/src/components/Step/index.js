import React, {Component} from "react";

import styles from "./step.scss";

export default class Step extends Component {

  constructor(props) {
    super(props);

    this.state = {
      scrolled: false,
      windowWidth: window.innerWidth,
    };
  }


  render() {
    const {show} = this.props;

    return (
      <div className={styles.step + ' ' + (show === true && styles.show)}>
        {this.props.children}
      </div>
    );
  }
}
