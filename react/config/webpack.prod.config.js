const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

var name = 'a';

function getName() {
	//
	// //name = ((parseInt(name, 35)+1).toString(36)).replace(/0/g,'a');
	//
	// console.log(name[0]);
	//
	// var number = name.length - 1;
	//
	// var add = true;
	//
	//
	//
	// for(var i = number; f = name[i]; i--) {
	//   if (add === true) {
	//     add = false;
	//
	//     var n = parseInt(name[i]) + 1;
	//
	//     console.log(n);
	//
	//     name[i] = n.toString();
	//   }
	// }
	//
	//
	// return 'c' + name;


	function nextChar(c) {
		var u = c.toUpperCase();
		if (same(u, 'Z')) {
			var txt = '';
			var i = u.length;
			while (i--) {
				txt += 'A';
			}
			return (txt + 'A');
		} else {
			var p = '';
			var q = '';
			if (u.length > 1) {
				p = u.substring(0, u.length - 1);
				q = String.fromCharCode(p.slice(-1).charCodeAt(0));
			}
			var l = u.slice(-1).charCodeAt(0);
			var z = nextLetter(l);
			if (z === 'A') {
				return p.slice(0, -1) + nextLetter(q.slice(-1).charCodeAt(0)) + z;
			} else {
				return p + z;
			}
		}
	}

	function nextLetter(l) {
		if (l < 90) {
			return String.fromCharCode(l + 1);
		}
		else {
			return 'A';
		}
	}

	function same(str, char) {
		var i = str.length;
		while (i--) {
			if (str[i] !== char) {
				return false;
			}
		}
		return true;
	}

	name = nextChar(name);

	console.log(name);

	return name.toLowerCase();
}

module.exports = {
	//https://webpack.js.org/configuration/devtool/#devtool
	devtool: 'source-map',
	//devtool: 'cheap-module-eval-source-map',

	//https://webpack.js.org/configuration/entry-context/#context
	context: path.join(__dirname, '../src'),

	entry: [
		'./index.js',
	],
	output: {
		path: path.join(__dirname, '../static'),
		filename: 'j.js',
	},
	module: {
		loaders: [
			{
				test: /node_modules/,
				loader: 'ify-loader',
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				loader: 'file-loader?name=public/fonts/[name].[ext]',
			},
			{
				test: /\.(jpg|png)$/,
				loader: 'url-loader?name=images/[name].[ext]?[hash:5]',
				options: {
					limit: 2500,
				},
			},
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: ['babel-loader'],
			},
			{
				test: /\.(scss|sass)$/,
				loader: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					loader: [
						'css-loader?localIdentName=[local]__[path][name]__[hash:base64:5]&modules&importLoaders=1&sourceMap',
						'postcss-loader',
						'sass-loader',
					],
				}),
			},
			{
				test: /\.(css)$/,
				loaders: [
					'style-loader',
					'css-loader',
				],
			},

		],
	},
	resolve: {
		extensions: ['.js'],
		modules: [
			path.resolve('src'),
			'node_modules',
		],
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			children: true,
			minChunks: 2,
			async: true,
		}),
		new webpack.optimize.OccurrenceOrderPlugin(true),
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false,
		}),
		new webpack.DefinePlugin({
			'process.env': {NODE_ENV: JSON.stringify('production')},
		}),
		new HtmlWebpackPlugin({
			template: 'index.html',
			minify: {
				removeComments: true,
				collapseWhitespace: true,
				removeRedundantAttributes: true,
				useShortDoctype: true,
				removeEmptyAttributes: true,
				removeStyleLinkTypeAttributes: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true,
			},
			inject: true,
			hash: true,
		}),

		// Extract the CSS into a separate file
		new ExtractTextPlugin('s.css'),

		new webpack.optimize.UglifyJsPlugin({
			compressor: {
				warnings: false,
			},
		}),

	],

	target: 'web', // Make web variables accessible to webpack, e.g. window

};
