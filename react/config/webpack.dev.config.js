const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('./');


module.exports = {
	//https://webpack.js.org/configuration/devtool/#devtool
	devtool: 'eval',
	//devtool: 'cheap-module-eval-source-map',

	//https://webpack.js.org/configuration/entry-context/#context
	context: path.join(__dirname, '../src'),

	entry: [
		'react-hot-loader/patch',
		'webpack/hot/only-dev-server',
		'./index.js',
	],
	output: {
		path: path.join(__dirname, '../static'),
		filename: '[name].js',
		chunkFilename: '[name].chunk.js',
	},
	module: {
		loaders: [
			// ERROR in ./~/{my lib}/~/linebreak/src/linebreaker.js
			// Module not found: Error: Cannot resolve module 'fs' in {my lib}/node_modules/linebreak/src
			//  @ ./~/{my lib}/~/linebreak/src/linebreaker.js 7:7-20
			// https://github.com/devongovett/linebreak/issues/1
			{
				test: /node_modules/,
				loader: 'ify-loader',
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				loader: 'file-loader?name=public/fonts/[name].[ext]',
			},
			{
				test: /\.(jpg|png)$/,
				loader: 'url-loader?name=[name].[ext]?[hash:5]',
				options: {
					limit: 2500,
				},
			},
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: ['babel-loader'],
			},
			{
				test: /\.(scss|sass)$/,
				loaders: [
					'style-loader',
					'css-loader?localIdentName=[local]__[path][name]__[hash:base64:5]&modules&importLoaders=1&sourceMap',
					'postcss-loader',
					'sass-loader',
				],
			},
			{
				test: /\.(css)$/,
				loaders: [
					'style-loader',
					'css-loader',
				],
			},

		],
	},
	resolve: {
		extensions: ['.js'],
		modules: [
			path.resolve('src'),
			'node_modules',
		],
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			//minChunks: Infinity,
			children: true,
			minChunks: 2,
			async: true,
		}),
		new webpack.DefinePlugin({
			'process.env': {NODE_ENV: JSON.stringify('development')},
		}),
		new HtmlWebpackPlugin({
			template: 'index.html',
			inject: true,
		}),
	],

	devServer: {
		contentBase: '../src',
		historyApiFallback: true,
		host: config.host,
		hot: true,
		port: config.port,
		stats: {
			cached: true,
			cachedAssets: true,
			chunks: true,
			chunkModules: false,
			colors: true,
			hash: false,
			reasons: true,
			timings: true,
			version: false,
		},
	},

	target: 'web', // Make web variables accessible to webpack, e.g. window

};
