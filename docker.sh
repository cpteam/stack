#!/usr/bin/env bash

ACTION=$1

PROJECT_NAME=$(basename $(pwd))
PROJECT_NAME=${PROJECT_NAME//-}

BASE_IP=172.21.101

DOCKER_TOOLS_VERSION=1.18

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DOCKER_COMPOSE_FILE=$DIR/dockerCompose.yml


if [ ! -e $DIR/_docker/tools/functions.sh ]; then
	echo "Docker-Tools missing!"
	echo "Download docker-tools"
	git clone https://gitlab.com/Luky/docker.git $DIR/_docker/tools >> $DIR/docker.sh.log 2>&1
fi

if [ ! -e $DIR/_docker/tools/functions.sh ]; then
	echo "Docker-Tools can't be downloaded"
	exit 1
fi

source _docker/tools/functions.sh

if [[ $DCKSHL_VERSION != $DOCKER_TOOLS_VERSION ]]; then
	cd $DIR/_docker/tools

	## Back compatibility
	if [ -n "$(type -t dockerShellSelfUpdate)" ] && [ "$(type -t dockerShellSelfUpdate)" = function ]
	then
		dockerShellSelfUpdate $DIR $DOCKER_TOOLS_VERSION
	else
		echo Docker-Tools is out-dated, use update method via new clone
		rm -rf $DIR/_docker/tools
		git clone https://gitlab.com/Luky/docker.git $DIR/_docker/tools >> $DIR/docker.sh.log 2>&1
	fi
	## Back compatibility END

	cd $DIR

	source _docker/tools/functions.sh
fi

dockerShellCompareVersions

# -----------------------------------------------------------------------------------------------------
# Parse Arguments
# -----------------------------------------------------------------------------------------------------

OPT_FORCE=0
OPT_BUILD=0
OPT_VERBOSE=0

for var in "$@"
do
	case "$var" in
		-f|--force)
			OPT_FORCE=1
			;;
		-v|--verbose)
			OPT_VERBOSE=1
			;;
		-b|--build)
			OPT_BUILD=1
			;;
		*)
			;;
	esac
done


# -----------------------------------------------------------------------------------------------------
# Base Actions
# -----------------------------------------------------------------------------------------------------

copyIdea

HOST_IP=$(ifconfig docker0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
eVerbose "XDEBUG" "Create XDEBUG_CONFIG"
eVerbose "XDEBUG" "Host IP: ${HOST_IP}"
echo "XDEBUG_CONFIG=remote_host=${HOST_IP}" > $DIR/.env.xdebug

eVerbose "Host address" "${BASE_IP}.20  cpteamstack.docker"

initAction $ACTION
actionExec $*

# -----------------------------------------------------------------------------------------------------
# Run Action
# -----------------------------------------------------------------------------------------------------

buildAction() {
	cp $DIR/_docker/tools/composer.sh $DIR/_docker/nette/composer.sh

	dockerBuild nette ${PROJECT_NAME}_nette $OPT_VERBOSE --rm=false --quiet
	dockerBuild nginx ${PROJECT_NAME}_nginx $OPT_VERBOSE --rm=false --quiet
#	dockerBuild node ${PROJECT_NAME}-node $OPT_VERBOSE --rm=false
#	dockerBuild react ${PROJECT_NAME}_react_1 $OPT_VERBOSE --rm=false

	if [ $DOCKER_HUB_LOGIN = 0 ] || [ $DOCKER_HUB_PASSWORD = 0 ]; then
		e "Ship" "Missing credentials in config.sh. Ship skipped."
	else
		docker login -u $DOCKER_HUB_LOGIN -p $DOCKER_HUB_PASSWORD
#		dockerShip webgarden/uuz1xltt5b $OPT_VERBOSE
		docker logout
	fi
}

runTests() {
	docker exec -i ${PROJECT_NAME}_nette_1 vendor/bin/codecept run "$@" \
		&& return 0 || return 1
}

runLint() {
	set -e
	docker exec -i ${PROJECT_NAME}_nette_1 vendor/bin/parallel-lint . \
		--exclude vendor \
		--exclude adminer-4.3.0-en.php \
		&& return 0 || return 1
}


runPhpstan() {
	docker exec -i ${PROJECT_NAME}_nette_1 vendor/bin/phpstan analyse src www tests "$@"
}


if [[ $ACTION == "build" ]]
then
	buildAction
	exit 0
fi

if [[ $ACTION == "cs" ]] || [[ $ACTION == "cbf" ]]
then
	clearCS() {
		rm --force $CS_STANDARD_PATH/Webgarden
		rm --force $CS_STANDARD_PATH/Webgarden7.1
		rm --force $CS_STANDARD_PATH/SlevomatCodingStandard
	}

	CS_STANDARD_PATH=$DIR/nette/vendor/squizlabs/php_codesniffer/CodeSniffer/Standards
	CS_OWN_PATH=./../../../../wglab/cs/standard

	CS_SLEVOMAT_PATH=./../../../../slevomat/coding-standard/

	# Clear before
	clearCS

	ln -s $CS_OWN_PATH/Webgarden $CS_STANDARD_PATH/
	ln -s $CS_OWN_PATH/Webgarden7.1 $CS_STANDARD_PATH/
	ln -s $CS_SLEVOMAT_PATH/SlevomatCodingStandard $CS_STANDARD_PATH/

	shift
	docker exec -it ${PROJECT_NAME}_nette_1 vendor/bin/php${ACTION} --standard=Webgarden7.1 src --colors $*

	# Clear after
	clearCS

	exit 0
fi

if [[ $ACTION == "lint" ]]
then
	shift

	set -e

	if [[ $* ]]
	then
		docker exec ${PROJECT_NAME}_nette_1 vendor/bin/parallel-lint $( echo $* | cut -c 7- ) --exclude vendor
	else
		runLint
	fi

	exit 0
fi

if [[ $ACTION == "pre-commit" ]]
then
	echo x
	#dockerExecStan ${PROJECT_NAME}_nette_1 analyse src www tests "$@"
fi

if [[ $ACTION == "tests" ]] || [[ $ACTION == "test" ]]
then
	runLint
	runTests
fi

if [[ $ACTION == "pre-push-internal" ]]
then
	e "skip"

#	if ! ( runLint && runTests)
#	then
#		notify-send "Push of '$PROJECT_NAME' failed" "There are some errors in test case." \
#		--urgency=critical \
#		-_nette_1-name=$PROJECT_NAME \
#		-t 0 \
#		-i face-sad
#
#		echo ""
#		echo "Test case failed, push has been rejected."
#
#		read -p "Press any key to exit > " -n1 junk
#		echo
#	else
#
#		echo ""
#		echo "Whole test case was sucessfully ended. This terminal will be closed in 5 seconds"
#		sleep 5
#	fi
fi


if [[ $ACTION == "pre-push" ]]
then
	e "skip"

#	x-terminal-emulator -e "$DIR/docker.sh pre-push-internal"
#
#	if ! ( runLint && runTests)
#	then
#		exit 1
#	fi
#	exit 0

fi

if [[ $ACTION == "run" ]]
then

	# stop composition
	dockerComposeStop $OPT_FORCE

	# touch empty files if doesn't exists
	touchFile ./nette/config/config.local.neon

	# create injector.sh files for...
	injector nette root www-data
	injector node root
	injector nginx root

	# build containers
	buildAction

	# run composition
	dockerComposeUp

	# connect containers to Intranet
	dockerIntranet ${PROJECT_NAME}_nginx_1 --ip ${BASE_IP}.20 --alias ${PROJECT_NAME}.docker
	dockerIntranet ${PROJECT_NAME}_nette_1 --ip ${BASE_IP}.10

	# attach to docker logs
	dockerLogs 5

	# exit with sucess status code
	exit 0
fi


if [[ $ACTION == "clear-cache" ]]
then
	rm -rf $DIR/nette/temp/cache/*
	rm -rf $DIR/nette/tempcli/cache/*

	exit 0
fi


if [[ $ACTION == "clear-locks" ]]
then
	sudo chown $USER -R *

	exit 0
fi


# shift ACTION arg (first one) away
shift

# create listeners for actions
actionPhp ${PROJECT_NAME}_nette_1 "$@"
actionComposer ${PROJECT_NAME}_nette_1 "$@"
#actionLint ${PROJECT_NAME}_nette_1 . "$@"
actionPhpstan ${PROJECT_NAME}_nette_1 analyse src www tests "$@" -c .phpstan.neon

actionHooks $ACTION

e "System" "Unknown command '$ACTION', valid are these:"
e "System"
e "System" "${CLR_GREEN}Project commands${CLR_END}"
e "System" " - run"
e "System" " - stop"
e "System"
e "System" "${CLR_GREEN}User commands${CLR_END}"
e "System" " - cs"
e "System" " - lint"
e "System" " - stan"
e "System"
e "System" "${CLR_GREEN}Shortcut commands${CLR_END}"
e "System" " - composer <cmd>"
e "System" " - php      <cmd>"
e "System" ""
e "System" "${CLR_GREEN}Environment commands${CLR_END}"
e "System" " - init      Init system environment"
e "System" " - install	 Install docker & docker compose"
e "System" " - network	 Create Intranet network (also stop all containers)"
e "System" " - hosts     Print all hosts for docker"
e "System" ""

exit 1
