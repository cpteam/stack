<?php declare(strict_types = 1);

const DIRECTORY_WWW = __DIR__;

$container = require __DIR__ . '/../src/bootstrap.php';

$app = $container->getByType(\Nette\Application\Application::class);
$app->run();
