<?php declare(strict_types = 1);

const TIMEZONE = 'Europe/Prague';

const DIRECTORY_TEMP = __DIR__ . '/../temp';
const DIRECTORY_LOG = __DIR__ . '/../log';
const DIRECTORY_CONFIG = __DIR__ . '/../config/config.neon';

const PATH_DEBUG_MODE = __DIR__ . '/debugMode.php';

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/shortcut.php';

$debugMode = false;
if (file_exists(PATH_DEBUG_MODE)) {
	$debugMode = require PATH_DEBUG_MODE;
}

$configurator = new Nette\Configurator;
$configurator->setTimeZone(TIMEZONE);

$configurator->setDebugMode($debugMode);

$configurator->enableDebugger(DIRECTORY_LOG);

$configurator->setTempDirectory(DIRECTORY_TEMP);

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(DIRECTORY_CONFIG);

return $configurator->createContainer();
