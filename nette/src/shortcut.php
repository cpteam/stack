<?php declare(strict_types = 1);

function dmp($x)
{
	\Tracy\Debugger::barDump($x);
}

function dd($x)
{
	\Tracy\Debugger::barDump($x);
}
