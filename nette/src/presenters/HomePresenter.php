<?php declare(strict_types = 1);

namespace App\Presenters;

use CPTeam\Forms\CoreForm;
use Nette\Application\UI\Form;

/**
 * @package App\Presenters
 */
class HomePresenter extends BasePresenter
{
	/* === ATTRIBUTES ========================================================================================= */
	
	/* === STARTUP ============================================================================================ */
	
	const RADIO_ITEMS
		= [
			'Red',
			'Blue',
			'Green',
		];
	
	public function startup(): void
	{
		parent::startup();
		
	}
	
	/* === ACTIONS ============================================================================================ */
	
	public function actionDefault(): void
	{
	
	}
	
	/* === HANDLES ============================================================================================ */
	
	/* === RENDERS ============================================================================================ */
	
	public function renderDefault(): void
	{
	
	}
	
	public function createComponentDummyForm(): CoreForm
	{
		$form = new CoreForm();
		
		$form->addText('name', 'Name')
			->setGridSize(6)
			->setAttribute('placeholder', 'Some placeholder...');
		
		$form->addText('textDisabled', 'Disabled text')
			->setDisabled(true)
			->setDefaultValue('Some disabled Text')
			->setGridSize(6);
		
		$form->addTextArea('text', 'Text')
			->setAutosize()
			->setAttribute('placeholder', 'Another placeholder...');
		
		$form->addCheckbox('check', 'Some check');
		
		$form->addRadioList(
			'radio',
			'Radio list',
			self::RADIO_ITEMS
		);
		
		$form->addSubmit('submit', 'Submit');
		
		return $form;
	}
}

