<?php declare(strict_types = 1);

namespace App\Presenters;

use Nette;

/**
 * @package App\Presenters
 * @property \App\Presenters\ITemplate $template
 */
abstract class Presenter extends Nette\Application\UI\Presenter
{
	use \Nextras\Application\UI\SecuredLinksPresenterTrait;
	
	public const WWW_TMP_PATH = '/webtemp';
	
	/** @var \CPTeam\Nette\Extensions\OpenGraph\OpenGraphFactory @inject */
	public $openGraphFactory;
	
	protected function startup(): void
	{
		parent::startup();
		
	}
	
	public final function handleLogout(): void
	{
		if ($this->user->isLoggedIn() === true) {
			$this->getUser()
				->logout(true);
		}
		
		$this->redirect('this');
	}
	
	protected final function createComponentOg(): \CPTeam\Nette\Extensions\OpenGraph\OpenGraphControl
	{
		return $this->openGraphFactory->create();
	}
	
}

